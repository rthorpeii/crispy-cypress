import feedFixture from '../fixtures/django/feed.json'

describe('Working with Feeds', () => {
    // Url paths used in this test
    const feedsUrl = Cypress.env('feeds_url')
    const myFeedsUrl = Cypress.env('my_feeds_url')

    context('Feeds page', () => {
        before(() => {
            cy.login()
            cy.visit(feedsUrl)
        })

        it('Lands on the expected page', () => {
            cy.validateLocation(feedsUrl)
        })

        it('Has all expected feeds present', () => {
            const numExpectedFeeds = Object.keys(feedFixture).length
            cy.get('table tbody tr').its('length').should('eq', numExpectedFeeds)
        })

        it('Has each feed correctly displayed', () => {
            cy.get('table tbody tr').each(($tr, index, $list) => {
                const currentFeed = feedFixture[index]
                cy.compareFeedRowToFixture($tr, currentFeed)
            })
        })
    })

    context('My Feeds Page', () => {
        before(() => {
            cy.login()
            cy.visit(myFeedsUrl)
        })

        // What feeds from the test fixture match user 1, who is currently logged in
        const myFeeds = feedFixture.filter(function(a){return (a.fields.added_by==1)});

        it('Lands on the expected page', () => {
            cy.validateLocation(myFeedsUrl)
        })

        it('Has all expected feeds present', () => {
            // Get the number of feeds added by user 1
            cy.get('table tbody tr').its('length').should('eq', myFeeds.length)
        })

        it('Has each feed correctly displayed', () => {
            cy.get('table tbody tr').each(($tr, index, $list) => {
                const currentFeed = myFeeds[index]
                cy.compareFeedRowToFixture($tr, currentFeed)
            })
        })
    })

})