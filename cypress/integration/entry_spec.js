import entryFixture from '../fixtures/django/feed_entry.json'
import feedFixture from '../fixtures/django/feed.json'

describe('Working with Entries', () => {
    // URLs used in this test
    const entryUrl = Cypress.env('entry_url')
    const feedsUrl = Cypress.env('feeds_url')

    // Utility method for validating the information displaed on a feed entry page.
    // $dl - The <dl> block containing the Feed, Author, URL, and Date Added information
    // testData - The json object which contains the fixture data which populated this entry
    const validateInfoBlock = ($dl, testData) => {
        // Validate the following fields of an entry: Feed, Author, URL
        // TODO: Validate the 'Date added' field
        cy.wrap($dl).get('dt').contains('Feed').then(($dt) => {
            // Determine what feed the testData belongs to
            const testFeed = feedFixture.filter(function (a) { return (a.pk == testData.fields.feed) })[0];
            // Validate the information displayed as the 'Feed'
            cy.wrap($dt).next().within(() => {
                cy.get('a').then((link) => {
                    // Text of Feed field
                    expect(link.text()).to.eq(testFeed.fields.title)
                    // Link value of Feed field
                    expect(link.prop('href')).to.eq(Cypress.config().baseUrl + Cypress.env('feeds_url') + testFeed.pk)
                })
            })
        })
        cy.wrap($dl).get('dt').contains('Author').then(($dt) => {
            cy.wrap($dt).next().should('have.text', testData.fields.author)
        })
        cy.wrap($dl).get('dt').contains('URL').then(($dt) => {
            cy.wrap($dt).next().within(() => {
                cy.get('a').then((link) => {
                    // Text of URL field
                    expect(link.text()).to.eq(testData.fields.url)
                    // Link value of Feed field
                    expect(link.prop('href')).to.eq(testData.fields.url)
                })
            })
        })
        // TODO: validate the data in the Date Added field
        cy.wrap($dl).get('dt').contains('Date added')
    }

    context('Entry is displayed correctly', () => {
        const testEntry = entryFixture[0]

        const demoEntryUrl = feedsUrl + testEntry.pk + entryUrl
        before(() => {
            cy.login()
            cy.visit(demoEntryUrl)
        })

        it('Lands on the expected page', () => {
            cy.validateLocation(demoEntryUrl)
        })

        it('Contains the expected entry information', () => {
            // Validate the header
            cy.get('header h1').should('have.text', testEntry.fields.title)
            // Validate the info block
            cy.get('dl[class=dl-horizontal]').then(($dl) => {
                validateInfoBlock($dl, testEntry)
            })
            // Has the content displayed in the page
            cy.get('div').contains(testEntry.fields.content)
        })
    })

    context('Adding a comment', () => {
        const demoEntryUrl = feedsUrl + 2 + entryUrl
        beforeEach(() => {
            cy.login()
            cy.visit(demoEntryUrl)
        })

        it('Lands on the expected page', () => {
            cy.validateLocation(demoEntryUrl)
        })

        it('Can add a comment', () => {
            cy.wait(1); // The comment box can take a bit of time to be fully rendered
            const commentText = "This is a comment, generated for testing this page"
            cy.get('pre[class*=CodeMirror-line] span').eq(0).type(commentText)
            cy.get('input[name=submit]').click()
            cy.get('p').contains(commentText)
        })
    })
})