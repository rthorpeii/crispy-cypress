import feedFixture from '../fixtures/django/feed.json'

describe('Working with Bookmarks', () => {
    const bookmarkedUrl = Cypress.env('bookmarked_url')
    const feedsUrl = Cypress.env('feeds_url')

    const testFeed = feedFixture[0]

    context('Adding and removing a bookmark', () => {
        before(() => {
            cy.login()
        })

        beforeEach(() => {
            Cypress.Cookies.preserveOnce('sessionid', 'csrftoken')
        })

        it('Bookmarked page lands on the expected page', () => {
            cy.visit(bookmarkedUrl)
            cy.validateLocation(bookmarkedUrl)
        })

        it('Bookmarked page has no entries at the start', () => {
            cy.visit(bookmarkedUrl)
            cy.get('div[class=container]').contains('Nothing to see here. Move on!')
        })

        it('Can add a bookmark from a feed', () => {
            cy.visit(feedsUrl + testFeed.pk)
            cy.get('p[class=text-right]').find('a[class=""]').click()
            cy.visit(bookmarkedUrl)
            cy.get('table tbody tr').eq(0).then(($tr) => {
                cy.compareFeedRowToFixture($tr, testFeed)
            })
        })

        it('Can remove a bookmark from a feed', () => {
            cy.visit(feedsUrl + testFeed.pk)
            cy.get('p[class=text-right]').find('a[class=""]').click()
            cy.visit(bookmarkedUrl)
            cy.get('div[class=container]').contains('Nothing to see here. Move on!')
        })
    })
})