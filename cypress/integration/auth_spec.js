describe('The Signup and Login Process', () => {
    // Username and password defined by cypress/fixtures/django/user.json
    const username = 'testUser'
    const password = 'demoPassword'

    // URLs used in this test
    const signupUrl = Cypress.env('signup_url')
    const loginUrl = Cypress.env('login_url')
    const feedsUrl = Cypress.env('feeds_url')

    // Utility function to fill the signup form
    const fillSignup = (username, password1, password2) => {
        cy.get('input[name=username]').type(username)
        cy.get('input[name=password1]').type(password1)
        cy.get('input[name=password2]').type(password2)
    }

    // Utility function to validate that we're logged in after signup/login
    const loggedIn = () => {
        // We are on the correct web page, according to the URL
        cy.validateLocation(feedsUrl)
        cy.getCookie('sessionid').should('exist')
        cy.get('a').contains('Logout').should('be.visible')
    }

    // Utility function to validate that we're not logged in after an invalid signup/login
    const notLoggedIn = (expectedPath) => {
        // We are on the correct web page, according to the URL
        cy.validateLocation(expectedPath)
        cy.getCookie('sessionid').should('not.exist')
        cy.get('a').contains('Login').should('be.visible')
    }

    context('Signup Page', () => {
        beforeEach(() => {
            cy.visit(signupUrl)
        })

        it('Lands on the expected page', () => {
            cy.validateLocation(signupUrl)
        })

        it('Can create a new user', () => {
            fillSignup('signupSuccess', 'dem0_password!', 'dem0_password!')
            cy.get('input[name=submit]').click()
            loggedIn()
        })

        it('rejects signup with non-matching passwords', () => {
            fillSignup('signupFail', 'password', 'not_the_same{enter}')
            notLoggedIn(signupUrl)
            // Error message is present
            cy.get('span[id=error_1_id_password2')
                .contains("The two password fields didn't match.")
                .should('be.visible')
        })

        it('rejects signup without username', () => {
            cy.get('input[name=password1]').type(password)
            cy.get('input[name=password2]').type(`${password}{enter}`)
            notLoggedIn(signupUrl)
            // Error message is present
            cy.get('span[id=error_1_id_username')
                .contains('This field is required.')
                .should('be.visible')
        })
    })

    // Utility function to fill the login form
    const fillLogin = (username, password) => {
        cy.get('input[name=username]').type(username)
        cy.get('input[name=password]').type(password)
    }

    context('Login page', () => {
        beforeEach(() => {
            cy.visit(loginUrl)
        })

        it('Lands on the expected page', () => {
            cy.validateLocation(loginUrl)
        })

        it('Sets CSRF cookie correctly', () => {
            let cookie
            cy.getCookie('csrftoken').should('exist')
                .then((c) => {
                    cookie = c
                })
            cy.get('input[name=csrfmiddlewaretoken]')
                .should('be.hidden')
                .then(elem => {
                    expect(elem.val()).to.equal(cookie.value)
                })
        })

        it('Logs in succesfully on form submission', () => {
            fillLogin(username, `${password}{enter}`)
            loggedIn()
        })

        it('Rejects an invalid username/password combo', function () {
            fillLogin(username, 'bad_password')
            cy.get('input[type=submit]').click()
            notLoggedIn(loginUrl)
        })
    })
})