This folder contains Django fixtures for populating testing data.

As part of the test data, the following two users are created, both with the password `demoPassword`
1. testUser
2. testUser2