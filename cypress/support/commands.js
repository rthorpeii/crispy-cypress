// ***********************************************
// Custom Cypress commands contained in this file:
// - compareFeedRowToFixture
// - login
// - validateLocation
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// Utility method for comparing a row from a table of feeds to the data expected from the test fixture
// $row the <tr> element containing the feed's information
// fixtureEntry the json object containing the fixture that populated $row
Cypress.Commands.add('compareFeedRowToFixture', ($row, fixtureEntry) => {
    cy.wrap($row).within(() => {
        // Check the title field
        cy.get('td').eq(0).get('a').then((link) => {
            // Text of the title
            expect(link.text()).to.eq(fixtureEntry.fields.title)
            // Link value
            expect(link.prop('href')).to.eq(Cypress.config().baseUrl + Cypress.env('feeds_url') + fixtureEntry.pk)
            // Link opens
            cy.request(link.prop('href'))
        })
        // Feed URL
        cy.get('td').eq(2).contains(fixtureEntry.fields.feed_url)
    })
})

// Custom command for logging in as user 1 'testUser' defined in cypress/fixtures/django/user.json
Cypress.Commands.add('login', () => {
    let csrf
    cy.request(Cypress.env('login_url'))
        .its('body')
        .then((body) => {
            const $html = Cypress.$(body)
            // Get the csrf token that is embedded in the login form
            csrf = $html.find('input[name=csrfmiddlewaretoken]').val()
        })
        .then(() => {
            cy.request({
                method: 'POST',
                url: Cypress.env('login_url'),
                form: true,
                body: {
                    username: 'testUser',
                    password: 'demoPassword',
                    csrfmiddlewaretoken: csrf,
                },
            })
        })
})

Cypress.Commands.add('validateLocation', (expectedPath) => {
    cy.location().should((loc) => {
        expect(loc.origin).to.eq(Cypress.config().baseUrl)
        expect(loc.pathname).to.eq(expectedPath)
    })
})