﻿Tests were executed between: 25/09 and 30/09 by Robert Thorpe
Authentication:
* Login
   * No issues encountered
* Signup
   * No issues encountered
* Logout
   * No issues encountered
* Browsing while not logged in
   * No issues encountered
Feeds
* Add Feeds
   * Able to add both of the following test feeds
      * http://www.nu.nl/rss/Algemeen
      * https://feeds.feedburner.com/tweakers/mixed
   * [BUG] Application does not gracefully fail if a URL that is not an RSS feed is provided. Instead of producing a user-friendly error,  a BrokenFeed exception is shown directly to the user. Steps to reproduce:
      * Navigate to http://localhost:8000/feeds/new/
      * Enter ‘http://localhost:8000/not/a/feed’ Into the text box, then click Submit.
* All Feeds
   * No issues encountered
* My Feeds
   * No issues encountered
* Bookmarked Page
   * No issues encountered
* Feed display
   * [BUG] Clicking the ‘Check for updates’ button doesn’t update the ‘Last checked’ field if there are no updates. Currently, clicking this button will only update the date and time displayed in ‘Last checked’ if there was an update.
* Feed Entries
   * [BUG] New lines in comments are not saved. Despite multiline comments displaying fine in both of the preview and side by side views of the comment editor, multiline comments are in a single line after being submitted.
* Header
   * No issues encountered
* Signup Banner
   * No issues encountered