# crispy-cypress
Cypress tests for Crispy Succotash

## Running the Tests
1. Place the crispy-cypress folder into the top level of your crispy-succotash folder.
    ```shell
    $ mv crispy-cypress <path_to_crispy-succotash>
    ```

2. Run NPM install from within crispy-cypress.
    ```shell
    $ cd crispy-cypress
    $ npm install
    ```

3. Launch Crispy Succotash using the testing configurations packaged with crispy-cypress.
    ```shell
    $ docker-compose -f ../docker-compose.yml -f cypress-run.yml up
    ```

4. Now that the server is running, run the cypres tests via NPM.
    ```shell
    $ npm run cy:run
    ```
    If you prefer to run the tests via the Cypress GUI, instead run the following to launch the GUI
    ```shell
    $ npm run cy:open
    ```

To re-run the tests, repeat steps 3-4
